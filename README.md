# README #

* G�n�rateur de Wordlist et cracker Wifi (WEP, WPA/WP2). Crunch-Cracker utilise crunch pour la cr�ation de liste et aircrack-ng pour le hack wifi...

* Wordlist g�n�rator and Wifi Cracker

**Crunch-Cracker 1.0 beta**

Function crunch simple Wordlist generator
 
* Date: 10/10/2016
* Dev: Shell
* BY: KURO

**Crunch-Cracker 1.1**

Automate cr�ateur de Wordlist

+ Setting menu Multilangual
+ Graphic

* Date: 23/04/2017
* Langage:ENG, FR, ESP
* Dev: Shell
* BY: KURO

**Crunch-Cracker 1.2**

Automate cr�ateur de Wordlist
Wifi cracker

+ Function Cracker 
+ Graphic

* Date: 29/04/2017
* Langage:ENG, FR, ESP
* Dev: Shell
* BY: KURO

# pr�-requis #

* Crunch
* Aircrack-ng (Fullpack)

# Utilisation # 

1. chmod +x crunchcracker.sh
2. ./crunchcracker.sh
